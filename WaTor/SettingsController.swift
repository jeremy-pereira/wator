//
//  SettingsController.swift
//  WaTor
//
//  Created by Jeremy Pereira on 08/09/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import Cocoa

protocol WorldOwner: class
{
	var worldSize: World.Vector { get }
	var initialFishCount: Int { get }
	var initialSharkCount: Int { get }
	var fishBreedingAge: Int { get }
	var sharkBreedingAge: Int { get }
	var sharkStarvationTime: Int { get }
	func reset(size: World.Vector, fishCount: Int, sharkCount: Int, fishBreedingAge: Int, sharkBreedingAge: Int, sharkStarvationTime: Int)
}

class SettingsController: NSObject
{
	@IBOutlet var settingsSheet: NSWindow!

	@IBOutlet weak var worldSizeX: NSTextField!
	@IBOutlet weak var worldSizeY: NSTextField!
	@IBOutlet weak var fishPopulation: NSTextField!
	@IBOutlet weak var sharkPopulation: NSTextField!
	@IBOutlet weak var fishBreedingAge: NSTextField!
	@IBOutlet weak var sharkStarvationTime: NSTextField!
	@IBOutlet weak var sharkBreedingAge: NSTextField!

	func makeWorld(sheetWindow: NSWindow, worldOwner: WorldOwner)
	{
		worldSizeX.integerValue = worldOwner.worldSize.x
		worldSizeY.integerValue = worldOwner.worldSize.y
		fishPopulation.integerValue = worldOwner.initialFishCount
		sharkPopulation.integerValue = worldOwner.initialSharkCount
		fishBreedingAge.integerValue = worldOwner.fishBreedingAge
		sharkBreedingAge.integerValue = worldOwner.sharkBreedingAge
		sharkStarvationTime.integerValue = worldOwner.sharkStarvationTime
		sheetWindow.beginSheet(settingsSheet)
		{
			switch $0
			{
			case .OK:
				worldOwner.reset(size: World.Vector(self.worldSizeX.integerValue, self.worldSizeY.integerValue),
								 fishCount: self.fishPopulation.integerValue,
								 sharkCount: self.sharkPopulation.integerValue,
								 fishBreedingAge: self.fishBreedingAge.integerValue,
								 sharkBreedingAge: self.sharkBreedingAge.integerValue,
								 sharkStarvationTime: self.sharkStarvationTime.integerValue)
			default:
				break
			}
		}
	}

	@IBAction func okPressed(sender: Any)
	{
		settingsSheet.sheetParent?.endSheet(settingsSheet, returnCode: .OK)
	}

	@IBAction func cancelPressed(sender: Any)
	{
		settingsSheet.sheetParent?.endSheet(settingsSheet, returnCode: .cancel)
	}

}
